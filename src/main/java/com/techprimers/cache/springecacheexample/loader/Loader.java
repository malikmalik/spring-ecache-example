package com.techprimers.cache.springecacheexample.loader;

import com.techprimers.cache.springecacheexample.model.Users;
import com.techprimers.cache.springecacheexample.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class Loader {

    @Autowired
    private UsersRepository usersRepository;

    @PostConstruct
    public void load(){
        List<Users> usersList= getList();
        usersRepository.save(usersList);
    }

    private List<Users> getList() {
        List<Users> usersList = new ArrayList<>();
        usersList.add(new Users("Zizou", "Real Madrid", 500000L));
        usersList.add(new Users("Pepe Guardiola", "Man City", 700000L));
        return usersList;
    }
}
