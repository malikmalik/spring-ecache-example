package com.techprimers.cache.springecacheexample.resource;

import com.techprimers.cache.springecacheexample.cache.UsersCache;
import com.techprimers.cache.springecacheexample.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/search")
public class UsersResource {

    @Autowired
    private UsersCache usersCache;

    @GetMapping(value = "/{name}")
    public Users getUser(@PathVariable final String name)
    {
        return usersCache.getUser(name);
    }
}
