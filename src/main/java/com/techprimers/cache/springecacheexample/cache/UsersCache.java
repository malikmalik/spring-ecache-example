package com.techprimers.cache.springecacheexample.cache;

import com.techprimers.cache.springecacheexample.model.Users;
import com.techprimers.cache.springecacheexample.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class UsersCache {

     @Autowired
     private UsersRepository usersRepository;

    @Cacheable(value = "usersCache", key = "#name")
    public Users getUser(String name) {
        return usersRepository.findByName(name);
    }
}
