package com.techprimers.cache.springecacheexample.repository;

import com.techprimers.cache.springecacheexample.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

    Users findByName(String name);

}
